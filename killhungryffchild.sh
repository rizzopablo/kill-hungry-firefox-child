# How much memory (%) should a firefox child eat to be killed?
max=40

for p in $( ps -eo pmem,pid,args --sort -pmem |grep childID | egrep -o '[0-9.]+ +[0-9]+' |sed -r 's/ +/:/g' )
do
    echo Testing: $p
    m=`echo $p|cut -d ':' -f 1`;
    if [ 1 -eq $(echo "$m > $max" | bc) ]
    then
	pid=$(echo $p|cut -d ':' -f 2);
	echo $pid
	kill $pid
    fi
done
